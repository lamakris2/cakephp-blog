<?php 
class Contact extends AppModel {

	 public $validate = array(
        'name' => array(
            'rule' => 'notEmpty'
        ),
        'e-mail' => array(
            'rule' => 'notEmpty'
        ),
        'website' => array(
            'rule' => 'notEmpty'
        ),
        'Message' => array(
            'rule' => 'notEmpty'
        )
    );
}
?>

<?php
class ContactsController extends AppController {
    

    	function beforeFilter() {
   		parent::beforeFilter();
    	$this->Auth->allow('*');
}	
public $helpers = array('Html', 'Form');

     public function admin_index() {
        $this->set('contacts', $this->Contact->find('all'));
    }

    public function admin_delete($id) {
    if ($this->request->is('get')) {
        throw new MethodNotAllowedException();
    }

    if ($this->Contact->delete($id)) {
        $this->Session->setFlash(
            __('The post with id: %s has been deleted.', h($id))
        );
    } else {
        $this->Session->setFlash(
            __('The post with id: %s could not be deleted.', h($id))
        );
    }

    return $this->redirect(array('action' => 'index'));
}
}
?>
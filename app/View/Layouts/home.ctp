<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title><?php echo $title_for_layout; ?></title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo Router::fullbaseUrl(); ?>/css/main.css">
</head>

<body>
	<div class="content">

		<div id="left">
				<div style="height:265px;"><img src="<?php echo Router::fullbaseUrl(); ?>/images/120.jpg"></div>
				<div class="menu"><a class="slist-group-item" href="/pages/profile" style="color: #D1D1D1"><span>Profile<i class="paddingFA fa fa-user"></i></span></a></div>
				<div class="menu"><a class="list-group-item" href="#" style="color: #D1D1D1"><span>Work<i class="paddingFA fa fa-briefcase"></i></span></a></div>
				<div class="menu"><a class="list-group-item" href="#" style="color: #D1D1D1"><span>Resume<i class="paddingFA fa fa-file-text"></i></span></a></div>
				<div class="menu"><a class="list-group-item" href="/posts" style="color: #D1D1D1" text-align: center><span>Blog<i class="paddingFA fa fa-comment"></i></span></a></div>
				<div class="menu"><a class="activeNavbar list-group-item" href="/" style="color: #D1D1D1"><span>Contact <i class="paddingFA fa fa-envelope"></i></span></a></div>
		</div>

		<div class="page">
			<h1 id="cont" style="margin-left: 30px; padding-top: 30px; font-weight: 600"><?php echo $title_for_layout; ?></h1>
			
				<div>
					<h4 id="next" style="font-weight: 100">Go to next / previous page</h4>
				</div>
				<div id="strel" style="margin-left: 600px; margin-top: -55px">
					<img src="<?php echo Router::fullbaseUrl(); ?>/images/strel.jpg">
			    </div>
			    

				<hr style="width: 640px; margin-left: 30px">

			
				<?php echo $this->fetch('content'); ?><br>

				

			
		
			<div class="footer">
				<ul class="navfooter">
					<li style="float:left;">&copy; 2014 Robb Armstrong, All Rights Reserved</li>
					<li style="float:right;">
						<i class="footerFA fa fa-facebook"></i>
						<i class="footerFA fa fa-twitter"></i>
						<i class="footerFA fa fa-dribbble"></i>
						<i class="footerFA fa fa-pinterest"></i>
					</li>
				</ul>
			</div>
		</div>
	</div>

</body>

</html>


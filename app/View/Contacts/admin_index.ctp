
<h1>CONTACTS</h1>
<table>
   <tr>
       <th>Id</th>
       <th>Name</th>
       <th>e-mail</th>
       <th>website</th>
       <th>Message</th>
       <th>DELETE</th>
   </tr>
   
   <?php foreach ($contacts as $contact): ?>
   <tr>
       <td><?php echo $contact['Contact']['id']; ?></td>
       <td>
           <?php echo $this->Html->link($contact['Contact']['name'],array('controller' => 'contacts', 'action' => 'view', $contact['Contact']['id'])); ?>
       </td>
       <td><?php echo $contact['Contact']['email']; ?></td>
       <td><?php echo $contact['Contact']['website']; ?></td>
       <td><?php echo $contact['Contact']['message']; ?></td>
       <td> 
            <?php
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete', $contact['Contact']['id']),
                    array('confirm' => 'Are you sure?')
                );
            ?>
        </td>

   </tr>
   <?php endforeach; ?>
   <?php unset($contact); ?>
</table>
